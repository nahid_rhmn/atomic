<?php

namespace App\Bitm\SEIP103963\Utility;

class Utility {
    
    static public function d ($param=false){
        echo "<pre>";
        var_dump($param);
        echo "</pre>";
    }
    
    static public function dd($param=false){
        self::d($param);
        die();
    }
    static public function redirect_book ($url="/atomicnahid/views/SEIP103963/book/list.php"){
            header ("Location:".$url);
    }
    
    static public function redirect_mobile ($url="/atomicnahid/views/SEIP103963/mobile/list.php"){
            header ("Location:".$url);
    }
    
    static public function redirect_checkbox ($url="/atomic/views/SEIP103963/checkbox/index.php"){
            header ("Location:".$url);
    }
    
     static public function redirect_checkmulti ($url="/atomic/views/SEIP103963/checkmulti/index.php"){
            header ("Location:".$url);
    }
        
     static public function redirect_day ($url="/atomic/views/SEIP103963/day/index.php"){
            header ("Location:".$url);
    }
       static public function redirect_email ($url="/atomicnahid/views/SEIP103963/email/list.php"){
            header ("Location:".$url);
    }
            static public function redirect_profilephoto ($url="/atomic/views/SEIP103963/profilephoto/index.php"){
            header ("Location:".$url);
    }
        static public function redirect_radio ($url="/atomic/views/SEIP103963/radio/index.php"){
            header ("Location:".$url);
    }
          static public function redirect_selectcity ($url="/atomic/views/SEIP103963/selectcity/index.php"){
              header ("Location:".$url);
    }
         static public function redirect_textarea ($url="/atomic/views/SEIP103963/textarea/index.php"){
              header ("Location:".$url);
    }
       static public function redirect_manager ($url="/atomicnahid/views/SEIP103963/manager/list.php"){
            header ("Location:".$url);
    }
}