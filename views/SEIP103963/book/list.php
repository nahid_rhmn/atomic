<?php

    include_once("../../../vendor/autoload.php");
    
    use \App\Bitm\SEIP103963\book\Book;
    
    $book = new Book();
    $books = $book->index();
    
    
    
?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="area">
        <div class="head fix">
            <div class="logo"> <h1>Logo </h1></div>
             <div class="home"><a href="index.html">Home </a> </div>
            
        </div>
         <div class="main_content">
             <form action="">
             <table class="tbl_list">
                 <tr>
                     <th>ID</td>
                     <th>Book Name</td>
                     <th>Author</td>
                     <th>Action</td>
                 </tr>
                 <?php
               $slno =1;
               foreach($books as $book){
               ?>
                 <tr>
                     <td><?php echo $book->id;?></td>
                     <td><?php echo $book->name;?></td>
                     <td><?php echo $book->author;?></td>

                     <td><a href="#"><img src="images/tick.png" alt="" /></a>
                     <a href="#"><img src="images/minus.png" alt="" /></a>
                     <a href="#"><img src="images/cross.png" alt="" /></a></td>
                 </tr>
                 
                <?php

            }
            ?>
                 
             </table>
                 </form>
             <div class="but">
             <ul>
                 <li><a href="list.html">List</a></li>
                 <li><a href="edit.html">Edit</a></li>
                 <li><a href="view.html">View</a></li>
            </ul>
                 </div>
        </div>
         <div class="footer">
                <p>Copyright &copy;  2015. SM Nahidur Rahman</p>
        </div>
    </div>
</body>
</html>