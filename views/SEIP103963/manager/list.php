<?php

    include_once("../../../vendor/autoload.php");
    
    use \App\Bitm\SEIP103963\manager\Manager;
    
    $manager = new manager();
    $managers = $manager->index();
    
    
    
?>

<!DOCTYPE HTML>
<html lang="en-US">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
    <div class="area">
        <div class="head fix">
            <div class="logo"> <h1>Logo </h1></div>
             <div class="home"><a href="index.html">Home </a> </div>
            
        </div>
         <div class="main_content">
             <form action="">
             <table class="tbl_list">
                 <tr>
                     <th>Weekday</th>
                     <th>Date</th>
                     <th>Manager</th>
                     <th>Qty</th>
                      <th>Action</th>
                 </tr>
                 <?php
               $slno =1;
               foreach($managers as $manager){
               ?>
                 <tr>
                     <td><?php echo $manager->weekday;?></td>
                     <td><?php echo $manager->dates;?></td>
                     <td><?php echo $manager->manager;?></td>
	<td><?php echo $manager->qty;?></td>

                     <td><a href="#"><img src="images/tick.png" alt="" /></a>
                     <a href="#"><img src="images/minus.png" alt="" /></a>
                     <a href="#"><img src="images/cross.png" alt="" /></a></td>
                 </tr>
                 
                <?php

            }
            ?>
                 
             </table>
                 </form>
             <div class="but">
             <ul>
                 <li><a href="list.html">List</a></li>
                 <li><a href="edit.html">Edit</a></li>
                 <li><a href="view.html">View</a></li>
            </ul>
                 </div>
        </div>
         <div class="footer">
                <p>Copyright &copy;  2015. SM Nahidur Rahman</p>
        </div>
    </div>
</body>
</html>